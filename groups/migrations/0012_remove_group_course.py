# Generated by Django 3.1 on 2020-10-13 21:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0011_auto_20201013_1455'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='course',
        ),
    ]
