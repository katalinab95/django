from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupCreateForm, GroupEditForm
from groups.models import Group


class GroupListView(ListView):
    paginate_by = 10
    page_kwarg = 'page'
    template_name = 'groups-list.html'
    context_object_name = 'groups'

    def get_queryset(self):
        groups = Group.objects.select_related('teacher', 'head').all()
        params = [
            'name',
            'course']
        for param in params:
            value = self.request.GET.get(param)
            if value:
                groups = groups.filter(**{param: value})

        return groups


class GroupEditView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupEditForm
    template_name = 'groups-edit.html'
    success_url = reverse_lazy('groups:list')
    pk_url_kwarg = 'id'

    def get_object(self, queryset=None):
        id = self.kwargs.get('id')         # noqa
        return self.get_queryset().get(id=id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.object.students.all()
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('groups:list')
    pk_url_kwarg = 'id'


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups-create.html'
    success_url = reverse_lazy('groups:list')
