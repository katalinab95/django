from django.urls import path

from groups.views import GroupCreateView, GroupDeleteView, GroupEditView, GroupListView

app_name = 'groups'

urlpatterns = [
    path('', GroupListView.as_view(), name='list'),
    path('edit/<int:id>', GroupEditView.as_view(), name='edit'),
    path('delete/<int:id>', GroupDeleteView.as_view(), name='delete'),
    path('create/', GroupCreateView.as_view(), name='create'),
    ]
