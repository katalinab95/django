import datetime
import random

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

# Create your models here.
from teachers.models import Teacher


class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(5)]
    )

    def __str__(self):
        return f'{self.name}, {self.floor}'


class Group(models.Model):
    name = models.CharField(max_length=64, null=True)
    start_date = models.DateField(null=True, default=datetime.date.today)
    teacher = models.ForeignKey(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='groups'
    )
    head = models.OneToOneField(
        to='students.Student',
        null=True,
        on_delete=models.SET_NULL,
        related_name='head_of_group'
    )
    classrooms = models.ManyToManyField(
        to=Classroom,
        related_name='groups'
    )

    def __str__(self):
        return f'{self.name}'

    @staticmethod
    def generate_group():
        group = Group(
            name=f'Group - {random.choice(range(5))}',
            course=random.choice([
                "LaunchCode's Discovery.",
                "Data Structures.",
                "Bioinformatics Algorithms.",
                "Linear Algebra: Problems and Methods.",
                "Kotlin.",
                "Atomic Kotlin.",
                "Easy Way to Technical Writing.",
                "Common English Verbs.",
            ]),
            teacher=random.choice(list(Teacher.objects.all()))
        )

        group.save()

    def clean(self):
        if self.head:
            if self.head not in self.students.all():
                raise ValidationError("Head of groups must be in group")

    def save(self, *args, **kwargs):
        # self.clean()
        super().save(*args, **kwargs)
