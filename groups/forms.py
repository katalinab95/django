from django import forms

from groups.models import Group


class GroupBaseForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'


class GroupCreateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        fields = ['name', 'start_date', 'teacher']


class GroupEditForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        pass
