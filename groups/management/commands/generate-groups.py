from django.core.management.base import BaseCommand

from groups.models import Group


class Command(BaseCommand):
    """
    generate groups
    """

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help='Количество создаваемых групп')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        for _ in range(count):
            Group.generate_group()
