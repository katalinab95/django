import datetime

from django.db import models

# Create your models here.


class Technology(models.Model):
    class CHOICE_TECHNOLOGY(models.IntegerChoices):
        GIT = 0, "Git"
        FLASK = 1, "Flask"
        DJANGO = 2, "Django"
        PYCHARM = 3, "Pycharm"

    name = models.IntegerField(choices=CHOICE_TECHNOLOGY.choices)

    def __str__(self):
        return f'{self.name}'


class Course(models.Model):
    name = models.CharField(max_length=64, null=True)
    start_date = models.DateField(null=True, default=datetime.date.today)
    tags = models.ManyToManyField(
        to=Technology,
        related_name='course'
    )

    def __str__(self):
        return f'{self.name}, {self.start_date}, {self.tags}'
