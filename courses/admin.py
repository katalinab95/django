from courses.models import Course, Technology

from django.contrib import admin


class TechnologyTable(admin.TabularInline):
    model = Technology.course.through


class CourseAdmin(admin.ModelAdmin):
    list_display = ['name', 'start_date']
    inlines = [TechnologyTable]


admin.site.register(Technology)
admin.site.register(Course, CourseAdmin)
