from django.urls import path

from students.views import (
    StudentCreateView,
    StudentDeleteView,
    StudentEditView,
    StudentListView,
)

app_name = 'students'

urlpatterns = [
    path('', StudentListView.as_view(), name='list'),
    path('create/', StudentCreateView.as_view(), name='create'),
    path('edit/<uuid>', StudentEditView.as_view(), name='edit'),
    path('delete/<uuid>', StudentDeleteView.as_view(), name='delete'),
    # path('generate/', generate_students, name='generate'),
    ]
