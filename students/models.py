import random

from core.models import BaseModel, Person

from django.core.exceptions import ValidationError
from django.db import models

from faker import Faker

from groups.models import Group


class Student(BaseModel, Person):
    rating = models.SmallIntegerField(null=True, default=0)
    phone_number = models.CharField(max_length=15, null=True)
    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    @staticmethod
    def generate_students(count):
        fake = Faker()
        result = []
        for _ in range(count):
            st = Student.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                birthdate=fake.date(),
                rating=random.randint(0, 100),
                email=fake.email(),
                group=random.choice(list(Group.objects.all()))
            )
            result.append(st)
            st.save()
        return result

    def save(self, *args, **kwargs):
        phone_number = self.phone_number
        if phone_number:
            if not phone_number.isnumeric():
                raise ValidationError("VALUE ERROR: must be integer")

        super().save(*args, **kwargs)
