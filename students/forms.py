from django import forms

from students.models import Student


class StudentBaseForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'rating', 'email', 'phone_number', 'group']


class StudentCreateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        fields = ['first_name', 'last_name', 'rating', 'email']


class StudentEditForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        fields = ['first_name', 'last_name', 'rating', 'email', 'phone_number', 'group']
