from django.contrib import admin # noqa

# Register your models here.
from students.models import Student


class StudentAdmin(admin.ModelAdmin):
    exclude = ['uuid']
    list_per_page = 10


admin.site.register(Student, StudentAdmin)
