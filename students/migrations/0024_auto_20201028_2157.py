# Generated by Django 3.1 on 2020-10-28 21:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0023_auto_20201008_2128'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='create_date',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='student',
            name='write_date',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
