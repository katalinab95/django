# Generated by Django 3.1 on 2020-09-18 22:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0016_auto_20200919_0140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='phone_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
