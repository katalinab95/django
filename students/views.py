from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentCreateForm, StudentEditForm
from students.models import Student


class StudentListView(ListView):
    paginate_by = 10
    page_kwarg = 'page'
    template_name = 'students-list.html'
    context_object_name = 'students'

    def get_queryset(self):
        students = Student.objects.select_related('group').all()
        params = ['first_name',
                  'last_name',
                  'rating']
        for param in params:
            value = self.request.GET.get(param)
            if value:
                students = students.filter(**{param: value})

        return students


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'students-create.html'
    success_url = reverse_lazy('students:list')


class StudentEditView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentEditForm
    template_name = 'students-edit.html'
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'uuid'

    def get_object(self, queryset=None):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'uuid'
