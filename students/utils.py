import random

from faker import Faker


fake = Faker()


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_length(request, default=10):
    length = request.GET.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def get_count(request):
    count = request.GET.get('count')
    if not count:
        raise ValueError('Count is required parameter')
    if int(count) != float(count):
        raise ValueError('Enter only integers')
    count = int(count)

    if not 0 < count <= 100:
        raise ValueError('Out of range')

    return int(count)


# def gen_student():
#     st = Student(first_name=fake.first_name(), last_name=fake.last_name(), birthdate=fake.date(),
#                  rating=random.randint(0, 100))
#     st.save()
#     return st
