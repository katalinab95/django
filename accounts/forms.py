from accounts.models import Profile

from django.contrib.auth.forms import PasswordChangeForm, UserChangeForm, UserCreationForm
from django.forms import ModelForm


class AccountCreateForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = ['username', 'first_name', 'last_name']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountEditPasswordForm(PasswordChangeForm):
    pass


class AccountProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'interests']
