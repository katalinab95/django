from accounts.models import Profile, UserActions

from django.contrib import admin


class ProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'image')
    list_display = ('user', 'image')


class UserActionAdmin(admin.ModelAdmin):
    fields = ('user', 'action')
    list_display = ('user', 'write_date', 'action')


admin.site.register(UserActions,  UserActionAdmin)
admin.site.register(Profile,  ProfileAdmin)
