from accounts.forms import AccountCreateForm, AccountEditPasswordForm, AccountProfileUpdateForm, AccountUpdateForm
from accounts.models import Profile, UserActions

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)

        messages.info(self.request, 'Successful create account')

        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)
        # try:
        #     profile = self.request.user.profile
        # except Exception:
        #     profile = Profile.objects.create(user=self.request.user)
        #     profile.save()
        #
        # user_act = UserActions.objects.create(user=self.request.user,
        #                                       action=UserActions.USER_ACTION.LOGIN,
        #                                       info='Login')
        #
        # user_act.save()

        messages.info(self.request, f'{self.request.user} Successful authorization')

        return result


class AccountLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'logout.html'

    def get(self, request, *args, **kwargs):
        result = super().get(request, *args, **kwargs)
        user = self.request.user

        messages.info(self.request, f'User {user} Successful logged out!')

        return result


class AccountUpdateView(LoginRequiredMixin, ProcessFormView):
    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )
        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile
        )

        if profile.image:
            user_image = Profile.objects.get(user=self.request.user)
            image = request.FILES['image']
            user_act = UserActions.objects.create(user=self.request.user,
                                                  action=UserActions.USER_ACTION.CHANGE_PROFILE_IMAGE,
                                                  info=f'{user_image.image} > {image}')
            user_act.save()

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            user_act = UserActions.objects.create(user=self.request.user,
                                                  action=UserActions.USER_ACTION.CHANGE_PROFILE,
                                                  info='Change Profile')

            user_act.save()

            messages.info(self.request, f' {self.request.user} Successful change profile')
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )


# class AccountUpdateView(LoginRequiredMixin, UpdateView):
#     model = User
#     template_name = 'profile.html'
#     form_class = AccountUpdateForm
#     success_url = reverse_lazy('core:index')
#
#     def get_object(self, queryset=None):
#         return self.request.user
#
    # def form_valid(self, form):
    #
    #     result = super().form_valid(form)
    #     user_act = UserActions.objects.create(user=self.request.user,
    #                                           action=UserActions.USER_ACTION.CHANGE_PROFILE,
    #                                           info='Change Profile')
    #
    #     user_act.save()
    #     messages.info(self.request, 'Successful change profile')
    #
    #     return result


class AccountChangePasswordView(LoginRequiredMixin, PasswordChangeView):
    form_class = AccountEditPasswordForm
    template_name = 'change-password.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        result = super().form_valid(form)
        user_act = UserActions.objects.create(user=self.request.user,
                                              action=UserActions.USER_ACTION.CHANGE_PASSWORD,
                                              info='Change Password')

        user_act.save()
        messages.info(self.request, 'Successful change password')

        return result
