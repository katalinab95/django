import copy
import time
from urllib.parse import urlencode

from core.models import Logger


class QueryParamsInjectorMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        query_params = copy.deepcopy(request.GET)
        if 'page' in query_params:
            del query_params['page']
        request.query_params = urlencode(query_params)

        response = self.get_response(request)

        return response


class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        query_params = copy.deepcopy(request.GET)

        start = time.time()

        response = self.get_response(request)

        stop = time.time()
        elapsed_time = stop - start
        if request.user.is_authenticated:
            logger = Logger.objects.create(user=request.user,
                                           path=request.path,
                                           execution_time=elapsed_time,
                                           query_params=query_params,
                                           info="middlewares")
            logger.save()

        return response
