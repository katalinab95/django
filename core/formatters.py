def format_int(value):
    return str(value)


def format_float(value):
    return str(value)


def format_list(lst):
    result = '<Empty list>'
    if lst:
        result = '<br>'.join([str(record) for record in lst])
    return result
