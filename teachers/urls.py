from django.urls import path

from teachers.views import TeacherCreateView, TeacherDeleteView, TeacherEditView, TeacherListView

app_name = 'teachers'

urlpatterns = [
    path('', TeacherListView.as_view(), name='list'),
    path('edit/<int:id>', TeacherEditView.as_view(), name='edit'),
    path('delete/<int:id>', TeacherDeleteView.as_view(), name='delete'),
    path('create/', TeacherCreateView.as_view(), name='create'),
    ]
