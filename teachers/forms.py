from django import forms

from teachers.models import Teacher


class TeacherBaseForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'birthdate', 'email']


class TeacherCreateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        fields = ['first_name', 'last_name']


class TeacherEditForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        fields = ['first_name', 'last_name', 'birthdate', 'email']
