from django.core.management.base import BaseCommand

from faker import Faker

from teachers.models import Teacher


fake = Faker()


class Command(BaseCommand):
    """
    generate teachers
    """

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help='Количество создаваемых пользователей')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        for _ in range(count):
            Teacher.objects.create(first_name=fake.first_name(),
                                   last_name=fake.last_name(),
                                   birthdate=fake.date(),
                                   email=fake.email())
