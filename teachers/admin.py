from django.contrib import admin # noqa

# Register your models here.
from groups.models import Group

from teachers.models import Teacher


class GroupTable(admin.TabularInline):
    model = Group
    raw_id_fields = ['teacher']
    fields = ['name', 'start_date', 'head', 'classrooms']
    readonly_fields = fields
    show_change_link = True


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name']
    fields = ['first_name', 'last_name', 'birthdate', 'email']
    inlines = [GroupTable]
    prefetch_related = ['groups']
    list_per_page = 15


admin.site.register(Teacher, TeacherAdmin)
