from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect  # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from teachers.forms import TeacherCreateForm, TeacherEditForm
from teachers.models import Teacher


class TeacherListView(ListView):
    paginate_by = 10
    page_kwarg = 'page'
    template_name = 'teachers-list.html'
    context_object_name = 'teachers'

    def get_queryset(self):
        teachers = Teacher.objects.all()
        params = [
            'first_name',
            'last_name',
        ]
        for param in params:
            value = self.request.GET.get(param)
            if value:
                teachers = teachers.filter(**{param: value})

        return teachers


class TeacherEditView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherEditForm
    template_name = 'teachers-edit.html'
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'id'

    def get_object(self, queryset=None):
        id = self.kwargs.get('id')    # noqa
        return self.get_queryset().get(id=id)

    def get_context_data(self, **kwargs):
        context = super(TeacherEditView, self).get_context_data(**kwargs)
        context['groups'] = self.object.groups.all()
        return context


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'id'


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers-create.html'
    success_url = reverse_lazy('teachers:list')
